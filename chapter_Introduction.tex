\chapter{Introduction}

  Nowadays, many applications of computer, cell phone or another device make use of the images as an essential resource. 
  This makes images a common part of people's daily lives, as well as being used in different fields and areas. 
  Important examples are medicine, video production, photography, remote sensing, and security monitoring.
  
  The vast majority of these applications to achieve their goal (e.g., face detection, fingerprint recognition, cell nucleus detection) share common steps such as image acquisition and image processing. 
  For this reason, they also present common problems for the treatment of images. One of these problems present in these applications is the noise, which hinders the correct visualization of the image and it is generally associated with the acquisition process. 
  Another important problem is to achieve improvement of certain features of the image, such as contour enhancement, which are useful for correctly delimiting the regions within the image and it is associated with image processing.
  These two problems, noise, and feature enhancement, present different importance for distinct areas, becoming relevant in, \eg, medical applications and video surveillance.
  Finally, an important point to consider is the problem of high computational cost employed in complex tasks, since these tasks are often used in a large dimensional space that requires a higher processing and memory resource of the computer.
  
  Due to these problems, the correct treatment of the images is necessary, emphasizing a pre-processing step in order to clean the image of unwanted components (\eg, the noise) and highlight the most important features of the image (\eg, the edges), in addition to taking into account the response time of the applications.
  
  Image processing and analysis area covers a large number of applications, ranging from lower-level tasks (\eg, extremum point detection) to more specialized tasks, such as segmentation and classification, that require the suppression of unnecessary details present in the input images. 
  
  Some techniques addressed these problems as part of the algorithm, but in many cases, it is common to apply a pre-processing step in the original image prior to any further processing. Within the pre-processing area, we focus on addressing these problems with the ``image simplification approach'', whereby the elimination of non-significant details is obtained in the image while keeping the information necessary to the achievement of the desired outcome, besides the reduction of response time.
  
  \section{Image Simplification Methods}
  
    The Image simplification method is commonly referred to as a pre-processing step and, in particular, Mathematical Morphology~\cite{Serr2003},\cite{Soille2013},\cite{Heijmans1998},\cite{Najman2013} introduces interesting low-level simplification filters exhibiting well-known properties~\cite{Serra1992}. 
	The typical filtering by opening and closing~\cite{Soille2013} and their combination as alternate sequential filters are commonly used to eliminate undesirable components of an image while preserving its main content.  
	
	Morphologically, the leveling approach~\cite{Meyer2004, Meyer1997} defines a reconstruction-based simplification without changes in the final contours \wrt, the original image.  
	Another example of morphological  simplification is the dynamic measure~\cite{Bertr2007} which selects components of an image according to the notion of  extinction values (\eg, area or volume)~\cite{Vachier2001}. 
	This procedure is closely related to the measure of persistence of a signal and is used to eliminate image components regarded as non-significant.
	The main limitation that shares the works mentioned above is the presence of a simple level of observation, limiting its capacity to extract information.
  
	To address this problem, the multi-scale approach is created which has multiple levels of observation. Within this approach, the scale-space theory~\cite{Witkin1984} defines a multi-level processing (from finer to coarser scales) related to different representations of the original signal. 
	In such a case, the simplification should be well-behaved in the sense that, given a certain feature of interest (\eg, the zero-crossing of a function), one seeks to track its representation along the different scales. 
	This multi-level transformation should satisfy some properties  of \textit{monotonicity}, \textit{continuity}, \textit{fidelity} and \textit{euclidean invariance}~\cite{Witkin1984}.
	The monotonicity concept guarantees the non-inclusion of new interest features at different scales; the continuity states that a continuous path should be defined by the remaining features along these scales; 
	the fidelity ensures that the signal tends to its original form as the scale tends to zero, and, finally, the euclidean invariance asserts that translation and rotation transformations result in translated and rotated signals.
     
    The method discussed here using this approach is based on scale-dependent non-flat structuring function~\cite{DORINI2013}, where a toggle-like transformation simplifies an image in a self-dual way.
    The drawback of using multi-scale methods is the lack of persistence measures in simplification.
  
    Recently, a non-self-dual simplification method is introduced that taken into account the relationship between image extrema~\cite{Polo2013}.
    More specifically, it considers the distance and contrast between the various regional maxima (minima) and define a total order relation closely linked to the degree of simplification one wants to impose. 
    As we will see later, this multi-level process establishes a non-decreasing and well-behaved activity from which the least significant extrema merge successively with the most significant ones, in a given neighborhood.
    This image simplification method presents interesting advantages (\eg, the measure of the persistence of the regional maxima or minima ) but its high computational cost makes this method an unfeasible option.
  
    In this Master Thesis, we explore a graph-based simplification method that guarantees a well-behaved suppression of the image extrema (regional maxima or minima) by taking into account both information of distance and contrast. 
    To achieve this simplification, we use some interesting properties of an approach that handles multiple levels of observation, called morphological scale-space.  
    By highlighting some new properties of the method, we define a local update of the graph which implies an interesting bypass in the whole algorithm structure reducing the time-consuming of the original algorithm. 
    Finally, we present some morphological applications where is illustrated how to combine this simplification process with well-known morphological tools to approach problems related mainly to multi-scale (multiple levels of observation) image segmentation, which is the process of partitioning a image into multiple segments. 
    This combination is illustrated in Fig.~\ref{fig:app_image_segmentation}, where at each level from Fig.~\ref{fig:seg1} to Fig.~\ref{fig:seg4} is increased the amount of less significant components that are eliminated.
    This application results in Fig.~\ref{fig:seg4}, which is composed only of significant components.  
    
  
      \begin{figure}[tb]      
  	  	\centering
  	  	\begin{subfigure}{3.9cm}
  	  		\centering
  	  		\includegraphics[width=3.7cm, height=3cm]{img/cel_deta_24_5_l_4_p.png}
  	  		\caption{~}
  	  		\label{fig:seg1}
  	  	\end{subfigure}%      
  	  	\begin{subfigure}{3.9cm}
  	  		\centering
  	  		\includegraphics[width=3.7cm, height=3cm]{img/cel_deta_4_58_l_17_p.png}
  	  		\caption{~}
  	  		\label{fig:seg2}
  	  	\end{subfigure}%      
  	  	\begin{subfigure}{3.9cm}
  	  		\centering
  	  		\includegraphics[width=3.7cm, height=3cm]{img/cel_deta_1_67_l_55_p.png}
  	  		\caption{~}
  	  		\label{fig:seg3}
  	  	\end{subfigure}%
  	  	\begin{subfigure}{3.9cm}
  	  		\centering
  	  		\includegraphics[width=3.7cm, height=3cm]{img/cel_deta_0_03_l_27_p.png}
  	  		\caption{~}
  	  		\label{fig:seg4}
  	  	\end{subfigure}
  	  	\caption[Application of successive simplifications using image segmentation]{Successive image simplification in different levels of observation from (a) to (d), combined with image segmentation.}
  	  	\label{fig:app_image_segmentation}
  	  \end{figure}
  
    Another application developed in this research is related with image homogenization, shown in Fig.~\ref{fig:app_image_homogenization}, where from the Fig.~\ref{fig:hom1} to Fig.~\ref{fig:hom5} the similar components of the image are merged dependent of the observation.
  
    \begin{figure}[tb]      
  	  	\centering
  	  	\begin{subfigure}{3.2cm}
  	  		\centering
  	  		\includegraphics[width=3cm, height=4.5cm]{img/bote_z.png}
  	  		\caption{~}
  	  		\label{fig:hom1}
  	  	\end{subfigure}%      
  	  	\begin{subfigure}{3.2cm}
  	  		\centering
  	  		\includegraphics[width=3cm, height=4.5cm]{img/bote_delta_59_00_l_3.png}
  	  		\caption{~}
  	  		\label{fig:hom2}
  	  	\end{subfigure}%      
  	  	\begin{subfigure}{3.2cm}
  	  		\centering
  	  		\includegraphics[width=3cm, height=4.5cm]{img/bote_delta_10_90_l_11.png}
  	  		\caption{~}
  	  		\label{fig:hom3}
  	  	\end{subfigure}%
  	  	\begin{subfigure}{3.2cm}
  	  		\centering
  	  		\includegraphics[width=3cm, height=4.5cm]{img/bote_delta_6_44_l_9.png}
  	  		\caption{~}
  	  		\label{fig:hom4}
  	  	\end{subfigure}%
  	  	\begin{subfigure}{3.2cm}
  	  		\centering
  	  		\includegraphics[width=3cm, height=4.5cm]{img/bote_delta_5_11_l_17.png}
  	  		\caption{~}
  	  		\label{fig:hom5}
  	  	\end{subfigure}
  	  	\caption[Application of successive simplifications using image homogenization]{ Successive image homogenization in different levels of observation from (a) to (e), using image simplification.}
  	  	\label{fig:app_image_homogenization}
  	  \end{figure}
  
  \newpage
  
  \section{Principal Contributions}
    The main contributions of this work are the following:
    
    \begin{itemize}
     \item The creation of a new image simplification method, based on the algorithm defined by Leite and Polo~\cite{Polo2013}. Our method removes noise and preserver the edges of the images.
     \item The introduction of new properties concerned with the graph created using the extrema relationship, resulting in the local update of a graph.
     \item The reduction of the steps necessary to perform the images simplification is performed by the creation of a bypass between the steps of the original approach.
     \item The reduction of the computational cost from the original approach, keeping its advantages.
     \item Several morphological applications are presented and discussed. In these applications we showed how to combine the simplification step with well-known morphological tools in applications related to image segmentation and homogenization.
    \end{itemize}

  \section{Organization}

    The text of this Master's Thesis is organized into six chapters, as follows.\\
    
	\textbf{Chapter \ref{chapter:MM} : \nameref{chapter:MM}}
	
	In this chapter, we make a brief introduction to the basic concepts of mathematical morphology, which are necessary for proper understanding of the remaining text. 
	Initially, some basic definitions of image transformations are presented, as well as some concepts of structuring functions and elements. 
	Also are presented the morphological operators used to define image transformations, in addition to a basic segmentation algorithm, but quite studied in the literature. 
	Finally, the techniques of image simplification are presented which use morphological operators.\\
	
	\textbf{Chapter \ref{chapter:Scale_Space} : \nameref{chapter:Scale_Space}}
	
	The main problems related to the use of multi-scale methods are due to the difficulty of linking significant and relevant information of a signal in 1D or a 2D image, through the different levels (scales). 
	In order to avoid these problems, Witkin~\cite{Witkin1984} proposes a new approach, called scale-space, that formalizes a set of properties allowing the consistent manipulation of different structures of the image. 
	With this representation, a feature of interest in the image describes a continuous path and it is possible to relate the information obtained in the different levels of observation, as well as to determine its exact location in the original signal.
	
	In this chapter, we present the most relevant concepts of the space-scale approach, either linear or 2D, as well as its main properties, which make possible the manipulation of the image, in a consistent way. 
	Finally, the techniques of images simplification are shown which make use of the morphological scale-space approach.\\
	
	\textbf{Chapter \ref{chapter:Method} : \nameref{chapter:Method}}
	
	One of the most recent approaches to simplifying images in mathematical morphology is given by Leite and Polo~\cite{Polo2013}, with the creation of a simplification method that takes into account the relationship between the extrema of the image (regional minima and maxima), based on separation and contrast. 
	
	In this chapter, we present a new image simplification method, based on the method of Leite and Polo~\cite{Polo2013}. 
	Although our method presents similar stages, we managed to address the main drawbacks of the original approach. 
	These problems are: 1) the high computational cost demanded at the time of executing the method and 2) the almost null exploration in the experimental area using this simplification method.
	
	We solve the first problem by a local update in the graph that supports whole the simplification process. 
	This is achieved by the creation of new properties, which have a direct influence on the graph.
	
	Initially, we discussed the method proposed by Leite and Polo~\cite{Polo2013} showing drawbacks in some stages and presenting our solutions. Subsequently, we present the new properties along with their demonstrations.\\
	
	\textbf{Chapter \ref{chapter:Results} : \nameref{chapter:Results}}
	
	We in this chapter, address the problem of almost no exploration in the experimental area using our method of simplification. 
	Here we illustrate some examples where we combine the process of image simplification of our method, with well-known morphological tools to address problems mainly related to the segmentation and homogenization of images multi-scale images.
	
	We show how to combine the image simplification method with watershed transformation, with morphological reconstruction, with the regions growth algorithm and finally, we present a gradient improvement by combining the gradients of the different scales generated.
	
	In addition, we present two types of results, the theoretical results, where we present the properties created in this research and the experimental results, where we compare the computational cost used by the Leite and Polo algorithm~\cite{Polo2013} and our algorithm. These experimental results are obtained using the databases BSDS300~\cite{Martin2001} and BSDS500~\cite{Arbelaez2011} databases.\\
	
	
	\textbf{Chapter \ref{chapter:Conclusions} : \nameref{chapter:Conclusions}}
	
	Finally, we present the conclusions obtained in the development of this research work, as well as the perspectives of future works.
	    
	  %\item \textbf{\autoref{chapter:MM} : \nameref{chapter:MM}}
    
%   \begin{table}
%     \caption[Shorter table caption]{Table caption caption caption caption
%       caption caption.}
%     \label{t:label}
%     \begin{center}
%       \begin{tabular}{|c|c|}
% 	\hline
% 	a & b \\\hline
% 	c & d \\\hline
%       \end{tabular}
%     \end{center}
%   \end{table}