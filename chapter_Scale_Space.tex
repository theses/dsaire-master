\chapter{Morphological Scale-Space}\label{chapter:Scale_Space}

  %When extracting characteristics of interest from an image, it is extremely important to consider that certain information only makes sense under certain observational conditions.
  In the introduction, we say that work with the morphological scale-space approach, this is due to the presence of interesting properties that make possible our simplification method. But to understand this approach, we first need to understand scale-space theory, its basic concepts, and its main properties.
  
  Remember, the images simplification aims to eliminate the non-significant components while maintaining the necessary details to achieve the desired outcome.
  In this sense, the basic morphological transformations on which the previous techniques are performed do not provide the level of detail necessary in their operations, since such details are only visible at a level of observation.
  For this reason, the scale-space approach was created, combining the multi-scale approach and the transformations of MM, to obtain different levels of observation.

  In image processing, is crucial that the features extraction from an image, and certain information only makes sense under some observation conditions (\eg, the distance of observation). This interpretation is related to the concept to scale.
  
  For a discrete function (\eg, a digital image), there are two different concepts to scale; the external scale used to indicate the size of the image and the internal scale to represent the sampling density.
  However, choosing the appropriate observation scale is not a trivial task. For that reason, the multi-scale approach was developed.
  
  \begin{figure}[bt]
  	\centering
  	\includegraphics[width=8cm]{img/multi-scale.pdf}
  	%\includegraphics[width=8cm]{img/multi-scale.png}
  	\caption[Multi-scale approach]{A multi-scale representation of a signal is an ordered set of derived signals intended to represent the original signal at different level of scale. Image taken from Lindeberg~\cite{Lindeberg1994}}
  	\label{fig:multi_scale_approach}
  \end{figure}

  The multi-scale approach, shown in Fig~\ref{fig:multi_scale_approach}, produces a set of signals whose structures are successively suppressed. In this way, it is possible to analyze the different levels of representation and to use those that only exhibit the interest features.
  
  Fig.~\ref{fig:example_multi_scale} shows one of the major problems of the multi-scale method. The difficulty of relating significant information of the signal through the different levels.
  Accordingly, the scale-space approach~\cite{Witkin1984} was created, showing in Fig.~\ref{fig:example_scale_space} that with the continuity property, guarantees a continuous path, of the remaining features, along these scales.
  Thus, it is possible to relate information obtained at different levels of observation, as well as to determine its precise location in the original signal.
  
  \begin{figure}[tb]      
  	\centering
  	\begin{subfigure}{6cm}
  		\centering
  		\includegraphics[width=4.6cm, height=4.3cm]{img/pyramid.pdf}
  		\caption{Multi-Scale}
  		\label{fig:example_multi_scale}
  	\end{subfigure}%      
  	\begin{subfigure}{6cm}
  		\centering
  		\includegraphics[width=4.6cm, height=4.3cm]{img/scale-space.pdf}
  		\caption{Scale-Space}
  		\label{fig:example_scale_space}
  	\end{subfigure}%      
  	\caption[Representation multi-scale and morphological scale-space]{The representation of the approach (a) multi-scale and (b) morphological scale-space.}
  	\label{fig:img_comparison_multi_scale}
  \end{figure}
  
  In this chapter, we describe some concepts and properties related to the scale-space theory~\cite{Weickert1999, Vachier2001, Bosworth2003}, which later on help us to understand our method of simplification. This chapter is distributed as follows.
  
  In Section~\ref{sec:linear_scale_space} we introduce the linear scale-space concept, with some properties. A two-dimensional scale-space is shown in Section~\ref{sec:two_dim_scale_space}. Subsequently, in Section~\ref{sec:morphological_scale_space}, we explain the morphological scale-space, as well as, its main properties. Finally, image simplification techniques using this morphological scale-space approach are presented in Section~\ref{sec:img_simplification_scale_space}.
  
  \section{Linear Scale-Space} \label{sec:linear_scale_space}
    
    The scale-space definition was developed for one-dimensional continuous signals, which generate a set of signals obtained by convolving the original signal with a Gaussian zero-mean kernel whose standard deviation represents the concept of scale.
    This concept is generally known as gaussian scale-space~\cite{Witkin1984, IIJIMA1962} and is defined as follows.
    
    \begin{definition}(Gaussian scale-space~\cite{Witkin1984, IIJIMA1962}).
    
		Let $f(x): \mathbb{R}^n \rightarrow \mathbb{R}$ be the original signal and let $p(x,\sigma): \mathbb{R}^n \times \mathbb{R} \rightarrow \mathbb{R}$ be the kernel of smoothing.
		The image scale-space $L:\mathbb{R}^n \times \mathbb{R} \rightarrow \mathbb{R}$ in the scale $\sigma$ is given by
		
		\begin{equation}
		  \Big[ L(x,\sigma) \Big](f) = f(x) \ast p(x,\sigma),
		\end{equation}
		
		where the symbol $\ast$ denotes the convolution operation and $p(x,\sigma)$ is the gaussian kernel~\cite{Jackway1996},    
		\begin{equation}
		  p(x,\sigma) = (2\pi\sigma^2)^{-1/2}exp\Big(-\frac{1}{2\sigma^2} x^2\Big).
		\end{equation}
    
    \end{definition}
    
    Let us recall that, the convolution process~\cite{Gonzalez2006} is defined as,
	\begin{equation}
	  w(x,y) * h(x,y) = \sum_{s=-a}^a \sum_{t=-b}^b w(s,t) h(x-s, y-t),
	\end{equation}
	where $w(x,y)$ is a kernel of size $m \times n$ and $a, b$ are the values used to move within the kernel, besides, $h(x,y)$ is the image. This equation evaluated for all values of the displacement variables $x$ and $y$ so that all elements of $w$ visit every pixel in $h$.

    The smoothing process should satisfy a set of properties. The conditions for the one-dimensional linear case, called scale-space theory axioms, are summarized below~\cite{Lindeberg1991, Lindeberg2013}:
    
    \begin{itemize}
     \item Invariance to translation, rotation and scale.
     \item Non-highlighting of local extremes.
     \item Non-creation of new extremes.
     \item Linearity and separability properties.
    \end{itemize}
    
  \section{Scale-Space on Two Dimensions} \label{sec:two_dim_scale_space}    
    
    Lifshitz, Lawrence and Pizer~\cite{Lifshitz1988} observed that when we want to extend the linear scale-space properties for two or more dimensions, any convolution kernel used to create a representation scale-space introduces new features in the scale changes.
    For this reason, a set of harder properties were considered, which constrain the composition of scale-space. Even so, it is not possible to completely prevent the increasing of new local extremes~\cite{Lindeberg1994}.

    One of these principal properties in a two-dimensional scale-space is created to preserve the monotonicity of the components. This property is called \textit{causality} defined for Koenderink~\cite{Koenderink1984}.
    According to which new level surfaces (curves) should not be created with increasing scale. In the discrete case, the local extremes must satisfy the non-enhancement property, where the value of a maximum must not increase or change (and a minimum should not decrease) for increasing scales.
    
    \begin{figure}[b]      
    	\centering
    	\begin{subfigure}{3.5cm}
    		\centering
    		\includegraphics[width=3cm, height=3cm]{img/gaussian_filter1.png}
    		\caption{}
    		\label{fig:gaussian_filter1}
    	\end{subfigure}%      
    	\begin{subfigure}{3.5cm}
    		\centering
    		\includegraphics[width=3cm, height=3cm]{img/gaussian_filter2.png}
    		\caption{}
    		\label{fig:gaussian_filter2}
    	\end{subfigure}%      
    	\begin{subfigure}{3.5cm}
    		\centering
    		\includegraphics[width=3cm, height=3cm]{img/gaussian_filter3.png}
    		\caption{}
    		\label{fig:gaussian_filter3}
    	\end{subfigure}%      
    	\begin{subfigure}{3.5cm}
    		\centering
    		\includegraphics[width=3cm, height=3cm]{img/gaussian_filter4.png}
    		\caption{}
    		\label{fig:gaussian_filter4}
    	\end{subfigure}%
    	\caption[Two dimensional scale-space]{ Example of gaussian two dimensional scale-space on (a) original image, using a scale equal to (b) two, (c) four and (d) eight.}
    	\label{fig:gaussian_filter}
    \end{figure}
    
    Even with this property, scale-space on two dimensions continue to present some problems.
    We can see in the Fig.~\ref{fig:gaussian_filter}, one the principal problems of the gaussian kernel for the two dimensional scale-space is the smoothing presented in the image after the convolution process.
 
    
  \section{Morphological Scale-Space} \label{sec:morphological_scale_space}
    The morphological scale-space approach is created in response to the problems mentioned in the previous sections, by extending linear scale-space to two dimensions. This morphological approach address the following problems:
    \begin{enumerate}
      \item The addition of new extremes in the different scales.
      \item The smoothing of the contours by the use of the gaussian kernel. 
      \item The translation of the extremes in the different scales.
    \end{enumerate}
    
    In Fig.~\ref{fig:Difference_multiscale_Mscapespace}, we can observe a comparison the multi-scale approach in Fig.~\ref{fig:img_pyramid} (rescaled to the same size in Fig.~\ref{fig:img_pyramid_rescale}) and the morphological scale-space in Fig.~\ref{fig:img_scale_space}.
    In contrast to multi-scale approaches, such as pyramids, the scale-space representation preserves the same spatial resolution (same number of samples) at all scales. This feature allows immediate access to the data of interest at any scale without the need for additional processing.
    
    \begin{figure}[bt]      
      \centering
      \begin{subfigure}{3.5cm}
	\centering
	\includegraphics[width=3cm, height=8cm]{img/img_pyramid.png}
	\caption{}
	\label{fig:img_pyramid}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
	\centering
	\includegraphics[width=2.4cm, height=8cm]{img/img_pyramid2.png}
	\caption{}
	\label{fig:img_pyramid_rescale}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
	\centering
	\includegraphics[width=2.4cm, height=8cm]{img/img_scale_space.png}
	\caption{}
	\label{fig:img_scale_space}
      \end{subfigure}%      
      \caption[Difference between multi-scale and morphological scale-space approach]{ Difference between multi-scale and morphological scale-space approach, where (a) is the image pyramid, (b) is the image pyramid rescaled to the same size and (c) is the morphological scale-space. Images taken from Lindeberg~\cite{Lindeberg1994}.}
      \label{fig:Difference_multiscale_Mscapespace}
    \end{figure}
    

    %\subsection{Structuring function for multi-scale}
    \subsection{Morphological Scale-Space Properties}\label{subsec:morpho_scale_space_properties}
      
      The morphological scale-space approach address the problems previously mentioned, using some of its main properties~\cite{Witkin1984}, which we describe below.
      
      \begin{itemize}
		\item \textbf{Monotonicity:} The monotonicity concept guarantees the non-inclusion of new interest features at different scales.
		      
		\item \textbf{Continuity: } The continuity states that a continuous path should be defined by the remaining features along these scales.
		      
		\item \textbf{Fidelity:} The fidelity ensures that the signal tends to its original form as the scale tends to zero.
		      
		\item \textbf{Euclidean Invariance:} The euclidean invariance asserts that translation and rotation transformations result in translated and rotated signals.
      \end{itemize}
      
      Note, these properties ensure that when suppressing an extreme, this does not reappear on higher scales and that the remaining extremes not suffer translation effect or change of value. We can see this properties in Fig.~\ref{fig:scale_space_properties}.
      
      \begin{figure}[tb]      
      	\centering
      	\begin{subfigure}{4cm}
      		\centering
      		\includegraphics[width=3.5cm, height=3.5cm]{img/Monotonicity.png}
      		\caption{Monotonicity}
      		\label{fig:Monotonicity}
      	\end{subfigure}%      
      	\begin{subfigure}{4cm}
      		\centering
      		\includegraphics[width=3.5cm, height=3.5cm]{img/Continuity.png}
      		\caption{Continuity}
      		\label{fig:Continuity}
      	\end{subfigure}%      
      	\begin{subfigure}{4cm}
      		\centering
      		\includegraphics[width=3.5cm, height=3.5cm]{img/Invariance.png}
      		\caption{Fidelity and \\ Euclidean Invariance}
      		\label{fig:Invariance}
      	\end{subfigure}%
      	\caption[Example morphological scale-space Properties]{Morphological scale-space properties.}
      	\label{fig:scale_space_properties}
      \end{figure}
    
    \subsection{Scale-Dependent Structuring Function}\label{subsec:non-flat-SF}
    
	  In order to perform the morphological scale-scape operations, we need to know a variation of the flat structuring functions presented in the section~\ref{sec:SE_and_SF}, which due to the multi-scale nature of the scale-space approach must be structuring functions dependent on the observation scale.
    
      In the case of grey images, most applications consider only flat structuring functions. 
      However, \textit{non-flat structuring functions} $g_\sigma$, can be used to define scale-dependent morphological transformations, which allow multi-scale representation of the analyzed signal.
      The $g_\sigma : \mathcal{G}_\sigma \subset \mathbb{R}^2 \rightarrow \mathbb{R}$ is defined as,
      
      \begin{equation}
	      g_\sigma(x) = \abs{ \sigma } g( \abs{\sigma}^{-1} x ) : x \in \mathcal{G}_\sigma, \forall \sigma \neq 0,
      \end{equation}
      
      where $\mathcal{G}_\sigma = \{ x: \norm{x} < \mathcal{R} \}$ is te support regional of the function $g_\sigma$.
      To guarantee the behavior of a monotonically decreasing function along any radial direction from the origin (anti-convex), it must satisfy posterior conditions~\cite{Jackway1996},
      
      \begin{equation}
	      \abs{ \sigma } \rightarrow 0 \Rightarrow g_\sigma(x) \rightarrow \left\{ \begin{array}{ll}
	      0, & \text{if } x = 0, \\
	      -\infty, & \text{otherwise}.
	      \end{array}
	      \right.
      \end{equation}
      
      \begin{equation}
	      0 < \abs{\sigma_1} < \abs{\sigma_2} \Rightarrow g_{\sigma_1}(x) \leq g_{\sigma_2}(x) : \forall x \in \mathcal{G}_{\sigma_1}.
      \end{equation}
      
      \begin{equation}
	      \abs{\sigma} \rightarrow \infty \ \Rightarrow \ g_\sigma(x) \rightarrow 0.
      \end{equation}
      
      Moreover, to prevent the level shifting, we need to take into account,
      
      \begin{equation}\label{eq:scalespace}
	      \Sup_{t \in \mathcal{G}_\sigma }\{g_\sigma(t)\} = 0.
      \end{equation}
      
      To avoid horizontal translation effects, one must consider
      
      \begin{equation}\label{eq:scalespace2}
	      g_\sigma(0) = 0.
      \end{equation}
      
      A non-flat structuring function that we need to take into account, for the development of our work is the non-flat pyramid-shaped structuring function defined by Leite and Polo~\cite{Polo2013} and shown in Fig.~\ref{fig:no_flat_SF}, which equation is defined by, as
      
      \begin{equation}\label{eq:no_flat_SF}
	      g_{\sigma}(x,y) = -\sigma \max\Big[\big\{\abs{x}, \abs{y}\big\}\Big].
      \end{equation}
      
      \begin{figure}[tb]
      	\centerline{\includegraphics[scale=0.33]{img/StructuringFunction.pdf}}
      	\caption[Non-flat pyramid-shaped structuring function]{Non-flat pyramid-shaped structuring function $g_\sigma$.}
      	\label{fig:no_flat_SF}
      \end{figure}
      
      %It was also proved in \cite{Jackway1996} that the scale-space conditions ensuring no changes in the original gray-scale  and position of the remaining extrema, as well as the non-introduction of new extrema in the simplified signal, are also obtained from this type of structuring function whose shape is concave downward and monotonic decreasing along any radial direction from its origin~\cite{Jackway1996}. 
	  
    
    \subsection{Morphological Scale-Space Operators}\label{subsec:scale-space_operators}
    
      At the beginning of this Master Thesis, we say that our method carries out a suppression of extrema, that is, the elimination of  maxima and minima regional in the image. To understand how we perform this process, we need to know the definition of the non-flat scale-dependent structuring function $g_\sigma$, introduced in Subsection~\ref{subsec:non-flat-SF}, the scale-space operators presented in this subsection, and their main properties referred in Subsection~\ref{subsec:morpho_scale_space_properties}.
      
      The creation of the set of morphological scale-space transformations is achieved by employing of morphological operators of erosion $\varepsilon$, and dilation $\sigma$, presented in Section~\ref{sec:basic_operators}, using a structuring function $g_\sigma$.
      We now describe the adaptation of the basic morphological operators using non-flat structuring functions.
      
      \newpage
      
      \begin{definition}[Erosion scale-dependent~\cite{Jackway1996}]
      
		The erosion of the function $f(x)$ with the structuring function $g_{\sigma}(x)$, noted $[\varepsilon_{g_{\sigma}}(f)](x)$, is defined by,
		\begin{equation}
		  [\varepsilon_{g_{\sigma}}(f)](x) = \Inf_{t \in \mathcal{G} \cap \mathcal{D}_{-x}}\{f(x+t)-g_\sigma(t)\}.
		\end{equation}
      \end{definition}

      \begin{definition}[Dilation scale-dependent~\cite{Jackway1996}]
      
		The dilation of the function $f(x)$ with the structuring function $g_{\sigma}(x)$, noted $[\delta_{g_{\sigma}}(f)](x)$ is defined by,
		\begin{equation}
		  [\delta_{g_{\sigma}}(f)](x) = \Sup_{t \in \mathcal{G} \cap \mathcal{\check{D}}_{x}}\{f(x-t)+g_\sigma(t)\}.
		\end{equation}
      \end{definition}
      
      \noindent where $f:\mathcal{D} \subset \mathbb{R}^2 \to \mathbb{R}$ is the image function, $\mathcal{D}_x$ is the translate of $\mathcal{D}$, 
      $\mathcal{D}_x = \{ x + t : t \in \mathcal{D} \}$, and $\check{\mathcal{D}}_{x}$ is the reflection of $\mathcal{D}$. Finally, $g_{\sigma}: \mathcal{G}_{\sigma} \subset \mathbb{R}^2 \rightarrow \mathbb{R}$ is the scaled structuring function.
      An example of such a function is given by Van Den Boomgaard \etal~\cite{VanDenBoomgaard1996} where $g_\sigma (x) = - \frac{1}{2\sigma}x^2$, and $\sigma > 0$ is the scale parameter.
      
      The result of these operations depends on the origin of the structuring function $g_\sigma$. 
      To avoid level shifting and horizontal translation effects, respectively, we have to consider $\Sup_{x \in \mathcal{G}_\sigma }\{g_\sigma(x)\} = 0$ and $g_\sigma(0) = 0$, 
      defined by Jackway \etal~\cite{Jackway1996} and introduced in Subsection~\ref{subsec:non-flat-SF}.
      %\begin{equation}%\label{eq:scalespace}
		%\Sup_{x \in \mathcal{G}_\sigma }\{g(x)\} = 0 \text{ and } g(0) = 0.
      %\end{equation}

      Also was proved the conditions of scale-space to ensure the non-changes in the original gray-scale and position of the remaining extrema, as well as the non-introduction of new extrema in the simplified signal, are also obtained from this type of structuring function whose shape is concave downward and monotonic decreasing along any radial direction from its origin~\cite{Jackway1996}.

  \section{Techniques for Image Simplification} \label{sec:img_simplification_scale_space}
  
    In this section, we describe of the image simplification method using the morphological scale-space as the principal
    process.
  
    \begin{figure}[b]      
      \centering
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/lena.png}
		\caption{}
		\label{fig:lena}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/MMDE_neg.png}
		\caption{}
		\label{fig:MMDE_neg}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/MMDE_pos.png}
		\caption{}
		\label{fig:MMDE_pos}
      \end{subfigure}%
      \caption[Example of MMDE approach]{ The transformation of the image (a) with the MMDE method using the scales (b) $-0.5$ and (c) $0.5$ in the structuring functions.}
      \label{fig:MMDE_method}
    \end{figure}
    
    In the context of scale-space theory, combinations of erosion and dilation operations using non-flat structuring functions have proved to be efficient for the elimination of unwanted components.
    The following discusses some approaches proposed in the literature.
    
    \subsection{Multiscale Morphological Dilation Erosion - MMDE}
    
      Jackway \etal proposed a new scale-space method called Multi-scale Morphological Dilation and Erosion~\cite{Jackway1994,Jackway1996}, where the morphological scale-space is defined for both positive and negative scales. For positive scales is considered the morphological operation of dilation and for negative scales, the erosion operation.
      To achieve this separation, Jackway \etal make use of a morphological operator type toggle, defined by,
      
      \begin{equation}
		( f \otimes g_\sigma)(x) = \left\{ \begin{array}{lc}
		\big[ \delta_{g_\sigma}(f) \big](x), & \text{if } \sigma > 0, \\
		f(x), & \text{if } \sigma = 0, \\
		\big[ \varepsilon_{g_\sigma}(f) \big](x), & \text{if } \sigma < 0,
		\end{array}
		\right. \label{eq:toogle_MMDE}
      \end{equation}
    
	  where $f$ is the original image, $\big[ \delta_{g_\sigma}(f) \big](x)$ denotes a dilation operator and $\big[ \varepsilon_{g_\sigma}(f) \big](x)$ the erosion operator, the pixel in the position $x$ using the scale-dependent structuring function $g_\sigma$.
    
      Fig.~\ref{fig:MMDE_method} illustrates a possible scale-space representation generated by the MMDE\@. Note that, while the ``negative'' scales enhances the dark structures of the image. The positive scales have the opposite effect.
    
    \subsection{Method Based on Opening and Closing}
    
      To generate a scale-space from the morphological operations of opening and closing~\cite{Chen1989}, we need to use non-flat scale-dependent structuring functions, which have to possess the necessary and sufficient conditions to guarantee the monotonicity of the components, to achieves these properties; the structuring functions have to be of the anti-convex shape~\cite{Jang1991}.
      
      The fact that opening and closing operations constitute non-auto-dual morphological filters can make them sensitive to noise. Since the opening eliminates positive components and closing has the same effect on negative components. To address this problem, opening and closing can operations be sequentially alternated.
      
      Fig.~\ref{fig:opening_closing_scaledependent_method} shows a possible transformation of the morphological operations of opening in Fig.~\ref{fig:Babuino_opening} and closing in Fig.~\ref{fig:Babuino_closing} using a non-flat structural function dependent on the scale and combinations of both operators in Fig~\ref{fig:Babuino_OpeClo}.
      
      \begin{figure}[tb]      
      \centering
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/Babuino.png}
		\caption{}
		\label{fig:Babuino}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/Babuino_opening.png}
		\caption{}
		\label{fig:Babuino_opening}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/Babuino_closing.png}
		\caption{}
		\label{fig:Babuino_closing}
      \end{subfigure}%
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/Babuino_OpeClo.png}
		\caption{}
		\label{fig:Babuino_OpeClo}
      \end{subfigure}
      \caption[Examples of opening and closing operators scale-dependent]{ The morphological transformation scale-dependent of (a) the orignal image to achieved (b) opening, (c) closing and (d) a combination.}
      \label{fig:opening_closing_scaledependent_method}
    \end{figure}
      
    \subsection{Method Based on Scale-Dependent Non-Flat Structuring Function}
      Dorini and Leite's works~\cite{DORINI2013, Leite2006}  introduce a new image processing operator based on scaled versions of a signal defined by the morphological scale-space transformations for image segmentation. 
      This operator, defined on the scope of a toggle transformation, considers local pixel information (not only scale knowledge) to determine if each pixel should be processed by erosion or dilation operator scale-dependent. 
      
      In their work is used as structuring function $g(x,y)=-max\{x^2,y^2\}$, which scale-dependent representation is given by,
      \begin{equation}
		g_\sigma (x,y) = -\frac{1}{\abs{\sigma}}max\{x^2, y^2\},
		\label{eq:SF_SMMMT}
      \end{equation}
      where $\sigma$ is the scale of the structuring function and which shape is shown in Fig.~\ref{fig:SF_SMMT}.
      
      \begin{figure}[tb]
		\centering
		\includegraphics[width=4cm, height=3cm]{img/SF_SMMT.png}
		\caption[Structuring Function SMMT]{ Structuring function of SMMT. Image taken from Dorini and Leite~\cite{DORINI2013}. }
		\label{fig:SF_SMMT}
      \end{figure}
      
      Using toggle operators provides two main advantages~\cite{Soille2013}, (1) the primitives and (2) the decision rule. 
      The approach uses as primitives an extensive and an anti-extensive transformation, namely, the scale-dependent dilation and erosion. The decision rule involves, at a point $x$, the value $f(x)$ and the primitive results.
      
      Formally the toggle operator defined by Dorini and Leite is given by,
      
      \begin{equation}
		( f \oslash g_\sigma)^n(x) = \left\{ \begin{array}{lc}
		\psi_1^n(x), & \text{if } \psi_1^n(x) - f(x) < f(x) - \psi_2^n(x), \\
		f(x), & \text{if } \psi_1^n(x) - f(x) = f(x) - \psi_2^n(x), \\
		\psi_2^n(x), & \text{otherwise},
		\end{array}
		\right. \label{eq:toogle_SMMT}
      \end{equation}
      
      where $\psi_1^n(x) = ( f \oplus g_\sigma )^n$, that is the dilation of $f$ with the scale-dependent structuring function $g_\sigma$, $n$ times.
      In the same way, $\psi_2^n(x) = ( f \ominus g_\sigma )^n$.
      
      \begin{figure}[tb]      
      \centering
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/monarch.jpg}
		\caption{}
		\label{fig:monarch}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/monarch_s_neg_0_2_l_10.png}
		\caption{}
		\label{fig:monarch1}
      \end{subfigure}%      
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/monarch_s_1_5_l_10.png}
		\caption{}
		\label{fig:monarch2}
      \end{subfigure}%
      \begin{subfigure}{3.5cm}
		\centering
		\includegraphics[width=3cm, height=3cm]{img/monarch_s_3_5_l_10.png}
		\caption{}
		\label{fig:monarch3}
      \end{subfigure}
      \caption[Examples of SMMT]{ Examples of SMMT on (a) the original image, using scales $\sigma$, of (b) $-0.2$, (c) $-1.5$ and (d) $3.5$.}
      \label{fig:example_SMMT}
    \end{figure}
      
    In Fig.~\ref{fig:example_SMMT}, we can see an example of the toggle operator proposed (Eq.~\ref{eq:toogle_SMMT}) in different scales, using the morphological operators with the scale-dependent structuring function previous defined in Eq.~\ref{eq:SF_SMMMT}.
      
      %where a toogle-like transformation simplifies an image in a self-dual way.
    %\subsection{A non-self-dual simplification method}
  %\section{Conclusions}
  
    The previously image simplification techniques, using the morphological space-scale approach, have the main drawback of not having any condition or measure of persistence that guarantees that the remaining ends are the most significant, making it difficult to select a scale. 
    
  \section{Summary}
    In this chapter, we presented a brief review of space-scale theory, ranging from the necessary properties to some of the existing approaches in the literature. A greater emphasis was placed on the erosion and dilation operations multi-scale using a non-flat scale-dependent structuring function, which is an important part to understand the method developed in this work.
    
    Finally, we presented techniques of simplification of images using the space-scale morphological approach, which lack a measure of persistence that guarantees the remaining extrema are the most significant.
    
    Polo and Leite addressed this problem, with the creation of an image simplification method that first eliminates the least significant extremes, guaranteeing that the most significant ends are the last ones to be eliminated.
    The next chapter discusses in detail this technique on which our simplification method is based.